// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('oftalmoApp', ['ionic', 'oftalmoApp.controllers', 'oftalmoApp.services', 'ngCordova'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: "/tab",
    abstract: true,
    share: false,
    remove: false,
    templateUrl: "templates/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    share: false,
    remove: false,
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'CondutasCtrl'
      }
    }
  })
  
  .state('tab.nivel1', {
      url: '/nivel1/:id',
      share: false,
      remove: false,
      views: {
        'tab-dash': {
          templateUrl: 'templates/nivel1.html',
          controller: 'Nivel1Ctrl'
        }
      }
    })
    
  .state('tab.nivel2', {
      url: '/nivel2/:id',
      share: false,
      remove: false,
      views: {
        'tab-dash': {
          templateUrl: 'templates/nivel2.html',
          controller: 'Nivel2Ctrl'
        }
      }
    })
    
  .state('tab.nivel3', {
      url: '/nivel3/:id',
      share: false,
      remove: false,
      views: {
        'tab-dash': {
          templateUrl: 'templates/nivel3.html',
          controller: 'Nivel3Ctrl'
        }
      }
    })
    
  .state('tab.nivel4', {
      url: '/nivel4/:id',
      share: false,
      remove: false,
      views: {
        'tab-dash': {
          templateUrl: 'templates/nivel4.html',
          controller: 'Nivel4Ctrl'
        }
      }
    })
    
  .state('tab.texto', {
	  url: '/texto/:page',
      share: true,
      remove: false,
      views: {
        'tab-dash': {
          templateUrl: 'templates/texto.html',
          controller: 'TextoCtrl'
        }
      }
    })
    
  .state('tab.config', {
      url: '/config',
      share: false,
      remove: false,
      views: {
        'tab-config': {
          templateUrl: 'templates/config.html',
          controller: 'ConfigCtrl'
        }
      }
    })
    
  .state('tab.favoritos', {
	  share: false,
	  remove: false,
	  cache: false,
      url: '/favoritos',
      views: {
        'tab-favoritos': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'FavoritosCtrl'
        }
      }
    })
    
   .state('tab.favorito', {
	  share: false,
	  remove: true,
      url: '/favorito/:page',
      views: {
        'tab-favoritos': {
          templateUrl: 'templates/favorito.html',
          controller: 'FavoritoCtrl'
        }
      }
    })
    
    .state('tab.chat-detail', {
      share: false,
      remove: false,
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.friends', {
      url: '/friends',
      share: false,
      remove: false,
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'CategoriaCtrl'
        }
      }
    })

    .state('tab.remedios', {
      url: '/remedios/:id',
      share: false,
      remove: false,
      views: {
          'tab-friends': {
              templateUrl: 'templates/remedios.html',
              controller: 'RemediosCtrl'
          }
      }
    })

  .state('tab.account', {
    url: '/account',
    share: false,
    remove: false,
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');
})

.run(['$ionicPlatform', '$rootScope', function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
    //if(ionic.Platform.isAndroid()){
    	//$rootScope.serviceUrl = "file:///android_asset/www/";
	//} else if(ionic.Platform.isIOS()){
	//	$rootScope.serviceUrl = "/var/mobile/Applications/ba22ce8a3418526968532d9a2c526b6be803dc07/Oftapp.app";
	//}

  });
  
  $rootScope.$on('$stateChangeStart', function (event, next, current) {
	  
	  if(!next.share){
		  $rootScope.showShareButton = false;
	  } else{
		  $rootScope.showShareButton = true;
	  }
	  
	  if(!next.remove){
		  $rootScope.showRemoveFavoritos = false;
	  } else{
		  $rootScope.showRemoveFavoritos = true;
	  }
  });	
  
}]);
