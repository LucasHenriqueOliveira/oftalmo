angular.module('oftalmoApp.controllers', [])

	.controller('headerCtrl', ['$scope', '$rootScope', '$ionicActionSheet', '$cordovaPrinter', '$ionicPopup', '$cordovaToast', '$cordovaEmailComposer', '$state', 'Condutas', '$localstorage', '$filter', '$cordovaFile', 'Texto', function($scope, $rootScope, $ionicActionSheet, $cordovaPrinter, $ionicPopup, $cordovaToast, $cordovaEmailComposer, $state, Condutas, $localstorage, $filter, $cordovaFile, Texto) {

		$scope.showOptionsCondutas = function() {

			$ionicActionSheet.show({
				buttons: [
					{ text: '<i class="icon ion-ios7-star"></i> Adicionar aos Favoritos' },
					{ text: '<i class="icon ion-email"></i> Enviar por e-mail' },
					{ text: '<i class="icon ion-printer"></i> Imprimir' },
				],
				cancelText: 'Cancelar',
				buttonClicked: function(index) {
					if(index == 0){
						$scope.favoritos = $localstorage.getObject('favoritos');

						if(JSON.stringify($scope.favoritos) === '{}'){
							$localstorage.setObject('favoritos', [{
								id_conduta: $localstorage.get('id_conduta'),
								name_conduta: $localstorage.get('name_conduta'),
								nivel1: $localstorage.get('nivel1'),
								name_nivel1: $localstorage.get('name_nivel1'),
								nivel2: $localstorage.get('nivel2'),
								name_nivel2: $localstorage.get('name_nivel2'),
								nivel3: $localstorage.get('nivel3'),
								name_nivel3: $localstorage.get('name_nivel3'),
								nivel4: $localstorage.get('nivel4'),
								name_nivel4: $localstorage.get('name_nivel4'),
								texto: $localstorage.get('texto')
							}]);

							$cordovaToast.show('Adicionado com sucesso!', 'short', 'center');

						} else{
							var id_conduta = $localstorage.get('id_conduta');
							var name_conduta = $localstorage.get('name_conduta');
							var nivel1 = $localstorage.get('nivel1');
							var name_nivel1 = $localstorage.get('name_nivel1');
							var nivel2 = $localstorage.get('nivel2');
							var name_nivel2 = $localstorage.get('name_nivel2');
							var nivel3 = $localstorage.get('nivel3');
							var name_nivel3 = $localstorage.get('name_nivel3');
							var nivel4 = $localstorage.get('nivel4');
							var name_nivel4 = $localstorage.get('name_nivel4');
							var texto = $localstorage.get('texto');

							var found = $filter('filter')($scope.favoritos, {id_conduta: id_conduta,
								name_conduta: name_conduta,
								nivel1: nivel1,
								name_nivel1: name_nivel1,
								nivel2: nivel2,
								name_nivel2: name_nivel2,
								nivel3: nivel3,
								name_nivel3: name_nivel3,
								nivel4: nivel4,
								name_nivel4: name_nivel4,
								texto: texto}, true);

							if (found.length) {
								var alertPopup = $ionicPopup.alert({
									title: 'Favoritos',
									template: 'Este item já consta na lista de favoritos!'
								});
								alertPopup.then(function(res) {
									return true;
								});

							} else {
								$scope.favoritos.push({
									id_conduta: id_conduta,
									name_conduta: name_conduta,
									nivel1: nivel1,
									name_nivel1: name_nivel1,
									nivel2: nivel2,
									name_nivel2: name_nivel2,
									nivel3: nivel3,
									name_nivel3: name_nivel3,
									nivel4: nivel4,
									name_nivel4: name_nivel4,
									texto: texto
								});
								$localstorage.setObject('favoritos', $scope.favoritos);

								$cordovaToast.show('Adicionado com sucesso!', 'short', 'center');
							}
						}
					}

					if(index == 1){

						cordova.plugins.email.isAvailable(
							function (isAvailable) {

								if(isAvailable){
									Texto.getTexto($localstorage.get('texto')).then(function (data) {
										var file_conduta = "www/templates/condutas/" + data;

										$cordovaFile.readAsText(cordova.file.applicationDirectory, file_conduta)
											.then(function (result) {

												cordova.plugins.email.open({
													subject: 'Oftapp',
													body: result,
													isHtml:  true
												});

											}, function (error) {
												var alertPopup = $ionicPopup.alert({
													title: 'Email',
													template: 'Erro ao capturar o texto do email!'
												});
												alertPopup.then(function(res) {
													return true;
												});
											});
									});
								} else {
									var alertPopup = $ionicPopup.alert({
										title: 'Email',
										template: 'Não foi detectado nenhum gerenciador de email configurado no dispositivo!'
									});
									alertPopup.then(function(res) {
										return true;
									});
								}
							}
						);
					}

					if(index == 2){

						cordova.plugins.printer.isAvailable(
							function (isAvailable) {

								if(isAvailable){
									Texto.getTexto($localstorage.get('texto')).then(function (data) {
										var file_conduta = "www/templates/condutas/" + data;

										$cordovaFile.readAsText(cordova.file.applicationDirectory, file_conduta)
											.then(function (result) {

												var page = location.href;
												cordova.plugins.printer.print(page, file_conduta, function () {
													return true;
												});

											}, function (error) {
												var alertPopup = $ionicPopup.alert({
													title: 'Impressora',
													template: 'Erro ao capturar o texto para impressão!'
												});
												alertPopup.then(function(res) {
													return true;
												});
											});
									});
								} else {
									var alertPopup = $ionicPopup.alert({
										title: 'Impressora',
										template: 'Não foi detectado nenhum gerenciador de impressora configurado no dispositivo!'
									});
									alertPopup.then(function(res) {
										return true;
									});
								}
							}
						);
					}
					return true;
				}
			});
		};

		$scope.showOptionsFavoritos = function() {

			$ionicActionSheet.show({
				buttons: [
					{ text: '<i class="icon ion-ios7-star"></i> Remover dos Favoritos' },
					{ text: '<i class="icon ion-email"></i> Enviar por e-mail' },
					{ text: '<i class="icon ion-printer"></i> Imprimir' },
				],
				cancelText: 'Cancelar',
				buttonClicked: function(index) {
					if(index == 0){
						$scope.favoritos = $localstorage.getObject('favoritos');

						var id_conduta = $localstorage.get('id_conduta');
						var name_conduta = $localstorage.get('name_conduta');
						var nivel1 = $localstorage.get('nivel1');
						var name_nivel1 = $localstorage.get('name_nivel1');
						var nivel2 = $localstorage.get('nivel2');
						var name_nivel2 = $localstorage.get('name_nivel2');
						var nivel3 = $localstorage.get('nivel3');
						var name_nivel3 = $localstorage.get('name_nivel3');
						var nivel4 = $localstorage.get('nivel4');
						var name_nivel4 = $localstorage.get('name_nivel4');
						var texto = $localstorage.get('texto');

						var found = $filter('filter')($scope.favoritos, {id_conduta: id_conduta,
							name_conduta: name_conduta,
							nivel1: nivel1,
							name_nivel1: name_nivel1,
							nivel2: nivel2,
							name_nivel2: name_nivel2,
							nivel3: nivel3,
							name_nivel3: name_nivel3,
							nivel4: nivel4,
							name_nivel4: name_nivel4,
							texto: texto}, true);

						if (found.length) {
							for(var i = 0; i < $scope.favoritos.length; i++) {
								var obj = $scope.favoritos[i];

								if(found.indexOf(obj) !== -1) {
									$scope.favoritos.splice(i, 1);
									i--;
								}
							}
							$localstorage.setObject('favoritos', $scope.favoritos);

							$cordovaToast
								.show('Removido com sucesso!', 'short', 'center')
								.then(function(success) {
									$state.go('tab.favoritos');
								}, function (error) {
									$state.go('tab.favoritos');
								});

						} else{
							var alertPopup = $ionicPopup.alert({
								title: 'Favoritos',
								template: 'Ocorreu um erro. Favor tentar novamente!'
							});
							alertPopup.then(function(res) {
								return true;
							});
						}
					}

					if(index == 1){

						cordova.plugins.email.isAvailable(
							function (isAvailable) {

								if(isAvailable){
									Texto.getTexto($localstorage.get('texto')).then(function (data) {
										var file_conduta = "www/templates/condutas/" + data;

										$cordovaFile.readAsText(cordova.file.applicationDirectory, file_conduta)
											.then(function (result) {

												cordova.plugins.email.open({
													subject: 'Oftapp',
													body: result,
													isHtml:  true
												});

											}, function (error) {
												var alertPopup = $ionicPopup.alert({
													title: 'Email',
													template: 'Erro ao capturar o texto do email!'
												});
												alertPopup.then(function(res) {
													return true;
												});
											});
									});
								} else {
									var alertPopup = $ionicPopup.alert({
										title: 'Email',
										template: 'Não foi detectado nenhum gerenciador de email configurado no dispositivo!'
									});
									alertPopup.then(function(res) {
										return true;
									});
								}
							}
						);
					}

					if(index == 2){
						cordova.plugins.printer.isAvailable(
							function (isAvailable) {

								if(isAvailable){
									Texto.getTexto($localstorage.get('texto')).then(function (data) {
										var file_conduta = "www/templates/condutas/" + data;

										$cordovaFile.readAsText(cordova.file.applicationDirectory, file_conduta)
											.then(function (result) {

												var page = location.href;
												cordova.plugins.printer.print(page, file_conduta, function () {
													return true;
												});

											}, function (error) {
												var alertPopup = $ionicPopup.alert({
													title: 'Impressora',
													template: 'Erro ao capturar o texto para impressão!'
												});
												alertPopup.then(function(res) {
													return true;
												});
											});
									});
								} else {
									var alertPopup = $ionicPopup.alert({
										title: 'Impressora',
										template: 'Não foi detectado nenhum gerenciador de impressora configurado no dispositivo!'
									});
									alertPopup.then(function(res) {
										return true;
									});
								}
							}
						);
					}
					return true;
				}
			});
		};

	}])

	.controller('CondutasCtrl', ['$scope', '$rootScope', 'Condutas', 'Texto', '$state', '$localstorage', function($scope, $rootScope, Condutas, Texto, $state, $localstorage) {

		$scope.condutas = Condutas.all();

		$scope.searchCondutas = function() {
			var searchConduta = document.getElementById('searchConduta').value;
			Condutas.searchCondutas(searchConduta).then(
				function(matches) {
					$scope.condutas = matches;
				}
			);
		};

		$scope.proximoPasso = function(id_conduta, conduta, id_texto){

			$localstorage.set('id_conduta', id_conduta);
			$localstorage.set('name_conduta', conduta);
			$localstorage.set('nivel1', null);
			$localstorage.set('name_nivel1', null);
			$localstorage.set('nivel2', null);
			$localstorage.set('name_nivel2', null);
			$localstorage.set('nivel3', null);
			$localstorage.set('name_nivel3', null);
			$localstorage.set('nivel4', null);
			$localstorage.set('name_nivel4', null);
			$localstorage.set('texto', id_texto);

			$rootScope.titulo = conduta;
			if(!id_texto){
				$state.go('tab.nivel1', {"id":id_conduta});
			} else{
				Texto.getTexto(id_texto).then(function (data) {
					$state.go('tab.texto', {"page": data});
				});
			}
		};
	}])

	.controller('Nivel1Ctrl', ['$stateParams', '$scope', '$rootScope', 'Nivel', 'Texto', '$state', '$localstorage', function($stateParams, $scope, $rootScope, Nivel, Texto, $state, $localstorage) {
		$scope.niveis1 = Nivel.getNivel1($stateParams.id);
		$scope.titulo = $rootScope.titulo;

		$scope.proximoPasso = function(id_nivel1, nivel1, id_texto){

			$localstorage.set('nivel1', id_nivel1);
			$localstorage.set('name_nivel1', nivel1);
			$localstorage.set('nivel2', null);
			$localstorage.set('name_nivel2', null);
			$localstorage.set('nivel3', null);
			$localstorage.set('name_nivel3', null);
			$localstorage.set('nivel4', null);
			$localstorage.set('name_nivel4', null);
			$localstorage.set('texto', id_texto);

			if(!id_texto){
				$rootScope.titulo = nivel1;
				$state.go('tab.nivel2', {"id":id_nivel1});
			} else{
				Texto.getTexto(id_texto).then(function (data) {
					$state.go('tab.texto', {"page": data});
				});
			}
		};

	}])

	.controller('Nivel2Ctrl', ['$stateParams', '$scope', '$rootScope', 'Nivel', 'Texto', '$state', '$localstorage', function($stateParams, $scope, $rootScope, Nivel, Texto, $state, $localstorage) {
		$scope.niveis2 = Nivel.getNivel2($stateParams.id);
		$scope.titulo = $rootScope.titulo;

		$scope.proximoPasso = function(id_nivel2, nivel2, id_texto){

			$localstorage.set('nivel2', id_nivel2);
			$localstorage.set('name_nivel2', nivel2);
			$localstorage.set('nivel3', null);
			$localstorage.set('name_nivel3', null);
			$localstorage.set('nivel4', null);
			$localstorage.set('name_nivel4', null);
			$localstorage.set('texto', id_texto);

			if(!id_texto){
				$rootScope.titulo = nivel2;
				$state.go('tab.nivel3', {"id":id_nivel2});
			} else{
				Texto.getTexto(id_texto).then(function (data) {
					$state.go('tab.texto', {"page": data});
				});
			}
		};
	}])

	.controller('Nivel3Ctrl', ['$stateParams', '$scope', '$rootScope', 'Nivel', 'Texto', '$state', '$localstorage', function($stateParams, $scope, $rootScope, Nivel, Texto, $state, $localstorage) {
		$scope.niveis3 = Nivel.getNivel3($stateParams.id);
		$scope.titulo = $rootScope.titulo;

		$scope.proximoPasso = function(id_nivel3, nivel3, id_texto){

			$localstorage.set('nivel3', id_nivel3);
			$localstorage.set('name_nivel3', nivel3);
			$localstorage.set('nivel4', null);
			$localstorage.set('name_nivel4', null);
			$localstorage.set('texto', id_texto);

			if(!id_texto){
				$rootScope.titulo = nivel3;
				$state.go('tab.nivel4', {"id":id_nivel3});
			} else{
				Texto.getTexto(id_texto).then(function (data) {
					$state.go('tab.texto', {"page": data});
				});
			}
		};
	}])

	.controller('Nivel4Ctrl', ['$stateParams', '$scope', '$rootScope', 'Nivel', 'Texto', '$state', '$localstorage', function($stateParams, $scope, $rootScope, Nivel, Texto, $state, $localstorage) {
		$scope.niveis4 = Nivel.getNivel4($stateParams.id);
		$scope.titulo = $rootScope.titulo;

		$scope.proximoPasso = function(id_texto, nivel4){

			$localstorage.set('nivel4', $stateParams.id);
			$localstorage.set('name_nivel4', nivel4);
			$localstorage.set('texto', id_texto);

			Texto.getTexto(id_texto).then(function (data) {
				$state.go('tab.texto', {"page": data});
			});
		};

	}])

	.controller('TextoCtrl', ['$stateParams', '$scope', '$rootScope', '$state', function($stateParams, $scope, $rootScope, $state) {
		$scope.titulo = $rootScope.titulo;

		$scope.templateUrl = function() {
			return 'templates/condutas/' + $stateParams.page;
		};

	}])

	.controller('FavoritosCtrl', ['$scope', 'Favoritos', '$state', 'Texto', '$localstorage', function($scope, Favoritos, $state, Texto, $localstorage) {

		$scope.favoritos = Favoritos.getFavoritos();

		$scope.proximoPasso = function(favorito){

			$localstorage.set('id_conduta', favorito.id_conduta);
			$localstorage.set('name_conduta', favorito.name_conduta);
			$localstorage.set('nivel1', favorito.nivel1);
			$localstorage.set('name_nivel1', favorito.name_nivel1);
			$localstorage.set('nivel2', favorito.nivel2);
			$localstorage.set('name_nivel2', favorito.name_nivel2);
			$localstorage.set('nivel3', favorito.nivel3);
			$localstorage.set('name_nivel3', favorito.name_nivel3);
			$localstorage.set('nivel4', favorito.nivel4);
			$localstorage.set('name_nivel4', favorito.name_nivel4);
			$localstorage.set('texto', favorito.id);

			Texto.getTexto(favorito.id).then(function (data) {
				$state.go('tab.favorito', {"page": data});
			});
		};
	}])

	.controller('FavoritoCtrl', ['$stateParams', '$scope', function($stateParams, $scope) {

		$scope.templateUrl = function() {
			return 'templates/condutas/' + $stateParams.page;
		}

	}])

	.controller('ConfigCtrl', ['$scope', '$localstorage', '$ionicPopup', 'Versao', '$cordovaToast', '$cordovaEmailComposer', function($scope, $localstorage, $ionicPopup, Versao, $cordovaToast, $cordovaEmailComposer) {

		Versao.getVersao().then(function (data) {
			$scope.versao = data;
		});

		$scope.clearFavoritos = function(){

			$scope.favoritos = $localstorage.getObject('favoritos');
			if(JSON.stringify($scope.favoritos) === '{}'){

				var alertPopup = $ionicPopup.alert({
					title: 'Favoritos',
					template: 'Não existe favorito cadastrado.'
				});
				alertPopup.then(function(res) {
					return true;
				});

			} else{

				var confirmPopup = $ionicPopup.confirm({
					title: 'Favoritos',
					template: 'Deseja apagar todos os favoritos?'
				});
				confirmPopup.then(function(res) {
					if(res) {
						$localstorage.remove('favoritos');
						$cordovaToast.show('Todos os favoritos foram apagados.', 'short', 'center');
					} else {
						return false;
					}
				});
			}
		};

		$scope.sendEmail = function(){
			cordova.plugins.email.isAvailable(
				function (isAvailable) {

					if(isAvailable){
						cordova.plugins.email.open({
							to: 'contato@oftapp.com.br',
							subject: 'Oftapp - Comentários',
							body: '',
							isHtml:  true
						});
					} else {
						var alertPopup = $ionicPopup.alert({
							title: 'Email',
							template: 'Não foi detectado nenhum gerenciador de email configurado no dispositivo!'
						});
						alertPopup.then(function(res) {
							return true;
						});
					}
				}
			);
		};
	}])

	.controller('CategoriaCtrl', ['$scope', 'Remedios', '$rootScope', '$state', function($scope, Remedios, $rootScope, $state) {
		$scope.categorias = Remedios.all();
		$scope.remedios = [];

		$scope.searchRemedios = function() {
			var searchRemedio = document.getElementById('searchRemedio').value;
			Remedios.searchRemedios(searchRemedio)
				.then(
				function(matches) {
					if(searchRemedio){
						$scope.remedios = matches;
					} else{
						$scope.remedios = [];
					}
				}
			)
				.catch(
				function(fallback) {
					$scope.remedios = [];
				}
			);

		};

		$scope.proximoPasso = function(id_categoria, name_categoria){
			$rootScope.titulo = name_categoria;
			$state.go('tab.remedios', {"id": id_categoria});
		};
	}])

	.controller('RemediosCtrl', ['$stateParams', '$scope', '$rootScope', '$state', '$localstorage', 'Remedios', function($stateParams, $scope, $rootScope, $state, $localstorage, Remedios) {
		$scope.remedios = Remedios.getRemedios($stateParams.id);
		$scope.titulo = $rootScope.titulo;

	}]);