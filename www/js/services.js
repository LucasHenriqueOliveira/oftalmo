angular.module('oftalmoApp.services', [])


.factory('Condutas', ['$http', '$q', '$timeout', function($http, $q, $timeout) {

	var condutas = [];

	$http.get("lib/data.json").success(function (data) {
		data.condutas.forEach(function (conduta) {
			condutas.push(conduta);
        });
    });
	condutas.sort();

	return {
		all: function() {
			return condutas;
	    },
	    searchCondutas : function(searchFilter) {
	        var deferred = $q.defer();
	        var matches = condutas.filter( function(conduta) {
	          if(conduta.name.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
	        });
	
	        $timeout( function(){    
	           deferred.resolve( matches );
	        }, 100);
	
	        return deferred.promise;
	    }
	};
}])

.factory('Nivel', ['$http', function($http) {

	return {
	    getNivel1: function(id_conduta){
	    	var niveis1 = [];
	    	
	    	$http.get("lib/data.json").success(function (data) {
	    		data.nivel1.forEach(function (nivel1) {
	    			if(nivel1.id_conduta == id_conduta){
	    				niveis1.push(nivel1);
	    			}
	            });
	        });
	    	niveis1.sort();
	    	return niveis1;
	    },
	    getNivel2: function(id_nivel1){
	    	var niveis2 = [];
	    	$http.get("lib/data.json").success(function (data) {
	    		data.nivel2.forEach(function (nivel2) {
	    			if(nivel2.id_nivel1 == id_nivel1){
	    				niveis2.push(nivel2);
	    			}
	            });
	        });
	    	niveis2.sort();
	    	return niveis2;
	    },
	    getNivel3: function(id_nivel2){
	    	var niveis3 = [];
	    	$http.get("lib/data.json").success(function (data) {
	    		data.nivel3.forEach(function (nivel3) {
	    			if(nivel3.id_nivel2 == id_nivel2){
	    				niveis3.push(nivel3);
	    			}
	            });
	        });
	    	niveis3.sort();
	    	return niveis3;
	    },
	    getNivel4: function(id_nivel3){
	    	var niveis4 = [];
	    	$http.get("lib/data.json").success(function (data) {
	    		data.nivel4.forEach(function (nivel4) {
	    			if(nivel4.id_nivel3 == id_nivel3){
	    				niveis4.push(nivel4);
	    			}
	            });
	        });
	    	niveis4.sort();
	    	return niveis4;
	    }
	};
}])

.factory('Texto', ['$http', '$q', '$rootScope', function($http, $q) {
	
	return {
	    getTexto: function(id_texto){
	    	var deferred = $q.defer();
	    	$http.get("lib/data.json").success(function (data) {
	    		data.texto.forEach(function (texto) {
	    			if(texto.id === id_texto){
	    				deferred.resolve(texto.texto);
	    			}
	            });
	        });
	    	return deferred.promise;
	    }
	};
}])

.factory('Favoritos', ['$localstorage', function($localstorage) {
	
	return {
	    getFavoritos: function(){
	    	var arrFavoritos = [];
    		var favoritos = $localstorage.getObject('favoritos');
    		
    		if(JSON.stringify(favoritos) !== '{}'){
    			favoritos.forEach(function (favorito) {
        			var name = favorito.name_conduta;
        			
        			if(favorito.nivel1 != 'null'){
        				name = name + ' > ' + favorito.name_nivel1;
        			}
        			
        			if(favorito.nivel2 != 'null'){
        				name = name + ' > ' + favorito.name_nivel2;
        			}
        			
        			if(favorito.nivel3 != 'null'){
        				name = name + ' > ' + favorito.name_nivel3;
        			}
        			
        			if(favorito.nivel4 != 'null'){
        				name = name + ' > ' + favorito.name_nivel4;
        			}
        			
        			arrFavoritos.push({ name : name, id : favorito.texto, id_conduta : favorito.id_conduta, name_conduta : favorito.name_conduta,
        				nivel1: favorito.nivel1, name_nivel1: favorito.name_nivel1,  nivel2: favorito.nivel2, name_nivel2: favorito.name_nivel2,
        				nivel3: favorito.nivel3, name_nivel3: favorito.name_nivel3,  nivel4: favorito.nivel4, name_nivel4: favorito.name_nivel4});
                });
    		}
    		return arrFavoritos;
	    }
	};
}])

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    },
    remove: function(key){
    	$window.localStorage.removeItem(key);
    }
  }
}])

.factory('Versao', ['$http', '$q', function($http, $q) {
	
	return {
	    getVersao: function(){
	    	var deferred = $q.defer();
	    	$http.get("lib/data.json").success(function (data) {
	    		deferred.resolve(data.versao);
	        });
	    	return deferred.promise;
	    }
	};
}])

.factory('Remedios', ['$http', '$q', '$timeout', function($http, $q, $timeout) {
	var categorias = [];
	var remedios = [];
	
	$http.get("lib/data.json").success(function (data) {
		data.categoria.forEach(function (categoria) {
			categorias.push(categoria);
			categoria.remedios.forEach(function (remedio){
				remedios.push(remedio);
			});
        });
    });


	return {
		all: function() {
			return categorias;
	    },
	    searchRemedios : function(searchFilter) {
	        var deferred = $q.defer();
	        var matches = remedios.filter( function(remedio) {
	        	if(remedio.name.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
	        });
	
	        $timeout( function(){    
	           deferred.resolve( matches );
	        }, 100);
	
	        return deferred.promise;
	    },
		getRemedios: function(id_categoria){
			var remedios = [];
			categorias.forEach(function (categoria) {
				if(categoria.id == id_categoria){
					remedios = categoria.remedios;
				}
			});
			return remedios;
		}

	};
}]);